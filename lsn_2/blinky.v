`default_nettype	none

module blinky(i_clk, o_led);
    parameter CNT_WIDTH = 27;
    input wire i_clk;
    output wire o_led;

    reg [CNT_WIDTH-1:0] cnt;

    always @(posedge i_clk)
        cnt <= cnt + 1'b1;

    assign o_led = cnt[CNT_WIDTH-1];

endmodule
