
module top(o_led);

output wire o_led;
wire clk;

blinky blinky_inst(
    .i_clk(clk),
    .o_led(o_led)
);

ps ps_inst (
    .FCLK_CLK0(),     // output wire FCLK_CLK0
    .FCLK_CLK1(clk),  // output wire FCLK_CLK1
    .FCLK_RESET0_N(), // output wire FCLK_RESET0_N
    .FCLK_RESET1_N(), // output wire FCLK_RESET1_N
    .MIO(),           // inout wire [31 : 0] MIO
    .PS_SRSTB(),      // inout wire PS_SRSTB
    .PS_CLK(),        // inout wire PS_CLK
    .PS_PORB()        // inout wire PS_PORB
);

endmodule
