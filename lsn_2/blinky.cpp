
#include "obj_dir/Vblinky.h"
#include <iostream>
#include <verilated.h>
#include <verilated_vcd_c.h>

#ifndef TRACE_FILE
#define TRACE_FILE "trace.vcd"
#endif

static void tick(size_t tick_cnt, Vblinky *blinky, VerilatedVcdC *trace)
{
	blinky->eval();
	if (trace != NULL) {
		trace->dump(tick_cnt * 10 - 2);
	}

	blinky->i_clk = 1;
	blinky->eval();
	if (trace != NULL) {
		trace->dump(tick_cnt * 10);
	}

	blinky->i_clk = 0;
	blinky->eval();
	if (trace != NULL) {
		trace->dump(tick_cnt * 10 + 5);
		trace->flush();
	}
}

int main(int argc, char **argv)
{
	Verilated::commandArgs(argc, argv);
	Verilated::traceEverOn(true);
	auto trace = new VerilatedVcdC;

	auto blinky = new Vblinky;
	blinky->trace(trace, 99);
	trace->open(TRACE_FILE);

	auto last_led = blinky->o_led;
	auto cycle = 0;
	for (; cycle < (1 << 20); ++cycle) {
		tick(cycle + 1, blinky, trace);
		if (last_led != blinky->o_led) {
			std::cout
			    << "Cycle: " << cycle
			    << " led = " << static_cast<int>(blinky->o_led)
			    << std::endl;
		}
		last_led = blinky->o_led;
	}

	trace->close();

	delete trace;
	delete blinky;
	return 0;
}
