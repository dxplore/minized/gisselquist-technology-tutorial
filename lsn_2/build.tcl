set partNum xc7z007sclg225-1
set outputDir ./vivado
set srcDir ..

file mkdir $outputDir
set files [glob -nocomplain "$outputDir/*"]
if {[llength $files] != 0} {
    file delete -force {*}[glob -directory $outputDir *];
}

set_part $partNum

read_verilog $srcDir/top.v
read_verilog $srcDir/blinky.v
read_xdc $srcDir/minized.xdc

# create a minimal ps ip where the FCLKs are inline with default
# minized implementation
#
# FCLKs:
#  - FCLK0 50MHz
#  - FCLK1 100MHz
create_ip -vlnv xilinx.com:ip:processing_system7:5.5 -module_name ps
set_property -dict {
    CONFIG.PCW_FPGA_FCLK0_ENABLE {1}
    CONFIG.PCW_FPGA_FCLK1_ENABLE {1}
    CONFIG.PCW_CLK0_FREQ {50000000}
    CONFIG.PCW_CLK1_FREQ {100000000}
    CONFIG.PCW_FCLK_CLK0_BUF {TRUE}
    CONFIG.PCW_FCLK_CLK1_BUF {TRUE}
    CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR0 {8}
    CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR0 {4}
    CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR1 {4}
    CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR1 {4}
    CONFIG.PCW_FCLK0_PERIPHERAL_CLKSRC {IO PLL}
    CONFIG.PCW_FCLK1_PERIPHERAL_CLKSRC {IO PLL}
    CONFIG.PCW_EN_CLK0_PORT {1}
    CONFIG.PCW_EN_CLK1_PORT {1}
    CONFIG.PCW_EN_RST1_PORT {1}
    CONFIG.PCW_USE_M_AXI_GP0 {0}
    CONFIG.PCW_EN_DDR {0}
} [get_ips ps]
synth_ip [get_ips ps]

synth_design -top top -part xc7z007sclg225-1
write_checkpoint -force $outputDir/post_synth.dcp
report_timing_summary -file $outputDir/post_synth_timing_summary.rpt
report_utilization -file $outputDir/post_synth_util.rpt

opt_design
place_design
report_clock_utilization -file $outputDir/clock_util.rpt
if {[get_property SLACK [get_timing_paths -max_paths 1 -nworst 1 -setup]] < 0} {
    phys_opt_design
}
write_checkpoint -force $outputDir/post_place.dcp
report_utilization -file $outputDir/post_place_util.rpt
report_timing_summary -file $outputDir/post_place_timing_summary.rpt

route_design -directive Explore
write_checkpoint -force $outputDir/post_route.dcp
report_route_status -file $outputDir/post_route_status.rpt
report_timing_summary -file $outputDir/post_route_timing_summary.rpt
report_power -file $outputDir/post_route_power.rpt
report_drc -file $outputDir/post_imp_drc.rpt
write_verilog -force $outputDir/blinky_netlist.v -mode timesim -sdf_anno true
write_bitstream -force $outputDir/blinky.bit
