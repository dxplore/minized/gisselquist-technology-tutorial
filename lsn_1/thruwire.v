`default_nettype none

module thruwire(i_bt, o_led);
    input wire i_bt;
    output wire o_led;

    assign o_led = i_bt;
endmodule
