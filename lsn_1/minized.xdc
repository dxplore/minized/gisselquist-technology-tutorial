set_property PACKAGE_PIN E12 [get_ports {o_led}];
set_property IOSTANDARD LVCMOS33 [get_ports {o_led}];

set_property PACKAGE_PIN E11 [get_ports {i_bt}];
set_property IOSTANDARD LVCMOS33 [get_ports {i_bt}];
