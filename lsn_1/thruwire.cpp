
#include <iostream>
#include <verilated.h>
#include "obj_dir/Vthruwire.h"

int main(int argc, char **argv)
{
	Verilated::commandArgs(argc, argv);
	auto thruwire = new Vthruwire;

	for (auto cycle = 0; cycle < 20; ++cycle) {
		thruwire->i_bt = cycle & 0x01;
		thruwire->eval();

		std::cout << "Cycle: " << cycle <<
			", switch: " << static_cast<int>(thruwire->i_bt) <<
			", led: " << static_cast<int>(thruwire->o_led)
			<<std::endl;
	}


	delete thruwire;
	return 0;
}
